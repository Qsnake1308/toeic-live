## PART:: 1
sudo docker stop toeic-part-mysql
sudo docker stop toeic-part-web

## DOCSER
sudo docker stop toeic-docser-mysql
sudo docker stop toeic-docser-web

## USER
sudo docker stop toeic-user-mysql
sudo docker stop toeic-user-web

## METEOR
sudo docker stop toeic-meteor-web
