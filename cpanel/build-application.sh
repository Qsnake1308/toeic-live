## Load default config
SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

##############################################################
# BUILD::PART
##############################################################
echo "Build services::Part::MySQL Storage"
sudo docker run -d --name=toeic-part-mysql-storage \
    toeic-part/mysql \
    true

echo "Build services::Part::MySQL"
sudo docker run -d --name=toeic-part-mysql \
    -p $TOEIC_PART_MYSQL:3306 \
    --volumes-from=toeic-part-mysql-storage \
    toeic-part/mysql

echo "Build services::Part::Web"
sudo docker run -d --name=toeic-part-web \
    --link toeic-part-mysql:db \
    -p $TOEIC_PART_WEB:80 \
    -v $TOEIC_HOME/service/part/src/www/:/opt/www/ \
    toeic-part/web

##############################################################
# BUILD::DOCSER
##############################################################
echo "Build services::DOCSER::MySQL Storage"
sudo docker run -d --name=toeic-docser-mysql-storage \
    toeic-docser/mysql \
    true

echo "Build services::DOCSER::MySQL"
sudo docker run -d --name=toeic-docser-mysql \
    -p $TOEIC_DOCSER_MYSQL:3306 \
    --volumes-from=toeic-docser-mysql-storage \
    toeic-docser/mysql

echo "Build services::DOCSER::Web"
sudo docker run -d --name=toeic-docser-web \
    --link toeic-docser-mysql:db \
    --link toeic-part-web:part \
    -p $TOEIC_DOCSER_WEB:80 \
    -v $TOEIC_HOME/service/docser/src/www/:/opt/www/ \
    toeic-docser/web

##############################################################
# BUILD::USER
##############################################################
echo "Build services::USER::MySQL Storage"
sudo docker run -d --name=toeic-user-mysql-storage \
    toeic-user/mysql \
    true

echo "Build services::USER::MySQL"
sudo docker run -d --name=toeic-user-mysql \
    -p $TOEIC_USER_MYSQL:3306 \
    --volumes-from=toeic-user-mysql-storage \
    toeic-user/mysql

echo "Build services::USER::Web"
sudo docker run -d --name=toeic-user-web \
    --link toeic-user-mysql:db \
    -p $TOEIC_USER_WEB:80 \
    -v $TOEIC_HOME/service/user/src/www/:/opt/www/ \
    toeic-user/web

##############################################################
# BUILD::METEOR
##############################################################
echo "Build services::METEOR::Mongo Storage"
sudo docker run -d --name=toeic-meteor-mysql-storage \
    toeic-meteor/web \
    true

echo "Build services::METEOR::Web"
sudo docker run -d --name=toeic-meteor-web \
    --link toeic-part-web:part \
    --link toeic-docser-web:docser \
    --link toeic-user-web:user \
    -p $TOEIC_METEOR_WEB:3000 \
    --volumes-from=toeic-meteor-mysql-storage \
    -v $TOEIC_HOME/service/meteor/src/www/:/opt/www/ \
    toeic-meteor/web
