## PART:: 1
sudo docker start toeic-part-mysql
sudo docker start toeic-part-web

## DOCSER
sudo docker start toeic-docser-mysql
sudo docker start toeic-docser-web

## USER
sudo docker start toeic-user-mysql
sudo docker start toeic-user-web

## METEOR
sudo docker start toeic-meteor-web
