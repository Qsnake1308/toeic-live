################################################
# STOPPING
################################################
## PART
sudo docker stop toeic-part-mysql
sudo docker stop toeic-part-web

## METEOR
sudo docker stop toeic-meteor-web

## DOCSER
sudo docker stop toeic-docser-mysql
sudo docker stop toeic-docser-web

## USER
sudo docker stop toeic-user-mysql
sudo docker stop toeic-user-web

################################################
# REMOVING
################################################
## PART:: 1
sudo docker rm toeic-part-mysql
sudo docker rm toeic-part-web

## METEOR
sudo docker rm toeic-meteor-web

## DOCSER
sudo docker rm toeic-docser-mysql
sudo docker rm toeic-docser-web

## USER
sudo docker rm toeic-user-mysql
sudo docker rm toeic-user-web
