SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh

##################################################
# DOCKER::PART
##################################################
cd $TOEIC_HOME/service/part/cpanel/dev/docker/mysql/
sudo docker build -t toeic-part/mysql .

cd $TOEIC_HOME/service/part/cpanel/dev/docker/web/
sudo docker build -t toeic-part/web .

##################################################
# DOCKER::METEOR
##################################################
cd $TOEIC_HOME/service/meteor/cpanel/docker/
sudo docker build -t toeic-meteor/web .

##################################################
# DOCKER::DOCSER
##################################################
cd $TOEIC_HOME/service/docser/cpanel/dev/docker/mysql/
sudo docker build -t toeic-docser/mysql .

cd $TOEIC_HOME/service/docser/cpanel/dev/docker/web/
sudo docker build -t toeic-docser/web .

##################################################
# DOCKER::USER
##################################################
cd $TOEIC_HOME/service/user/cpanel/dev/docker/mysql/
sudo docker build -t toeic-user/mysql .

cd $TOEIC_HOME/service/user/cpanel/dev/docker/web/
sudo docker build -t toeic-user/web .
