SCRIPT_DIR=`dirname $0`
. $SCRIPT_DIR/config.sh
sudo docker exec -i toeic-meteor-web /bin/bash -c "export APP=${APP_NAME} && \
  cd /opt/www/${APP_NAME} && \
  meteor"
