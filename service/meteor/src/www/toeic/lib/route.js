FlowRouter.route('/', {
  action() {
    BlazeLayout.render('Main', {main: "Home"});
  }
});
FlowRouter.route('/part/1/description', {
  action() {
    BlazeLayout.render('Main', {main: "Description"});
  }
});
/*******************************************************
 *** PART 5
 *******************************************************/
FlowRouter.route('/part/5/exam', {
  action() {
    BlazeLayout.render('Main', {main: "PartFive"});
  }
});

/*******************************************************
 *** PART 6
 *******************************************************/
FlowRouter.route('/part/6/exam', {
  action() {
    BlazeLayout.render('Main', {main: "PartSix"});
  }
});

/*******************************************************
 *** PART 7
 *******************************************************/
FlowRouter.route('/part/7/exam', {
  action() {
    BlazeLayout.render('Main', {main: "PartSeven"});
  }
});
