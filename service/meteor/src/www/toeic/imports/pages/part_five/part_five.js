import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';
import {ReactiveMethod} from 'meteor/simple:reactive-method'
import React from 'react';
import Part from '/imports/react_components/Part_Five_Component.jsx';
import './part_five.html';

Template.PartFive.helpers({
  'Part':function() {
    return Part;
  },
  'questions': function() {
    return [
      { id: 1, part_no: 1,part_text: "Bla bla bla" },
      { id: 2, part_no: 2,part_text: "Bla bla bla" },
      { id: 3, part_no: 3,part_text: "Bla bla bla" },
      { id: 4, part_no: 4,part_text: "Bla bla bla" },
    ];
  }
})
