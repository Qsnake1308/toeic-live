import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import '/imports/components/part/part.js';
import '/imports/components/reading/reading.js';
import '/imports/components/listening/listening.js';
import '/imports/components/full/full.js';
import './home.html';
