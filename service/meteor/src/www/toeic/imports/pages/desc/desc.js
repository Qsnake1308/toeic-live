import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './desc.html';

Template.Description.events({
  'click .start-p5': function() {
    Meteor.call('five.get_questions', function(error, response) {
      if (response) {
        console.log(response);
      }
    });
  },
})
