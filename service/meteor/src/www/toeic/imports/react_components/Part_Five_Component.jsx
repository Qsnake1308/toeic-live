import React, { Component } from 'react';

export default class Part extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data
    };
  }
  renderBoxs() {
    return this.state.data.map(function (item) {
      return (<div className="part-five-qstn" key={item.id}>{item.part_text}</div>);
    });
  }
  render() {
      return (
        <div className="part-five-qstns">
            { this.renderBoxs() }
        </div>
      );
  }
}
