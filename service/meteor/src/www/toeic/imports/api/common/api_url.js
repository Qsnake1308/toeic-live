export default {
  part_url() {
    const PART_URL = 'http://'+process.env.PART_PORT_80_TCP_ADDR.toString()+":"+process.env.PART_PORT_80_TCP_PORT.toString();
    return PART_URL;
  }
}
