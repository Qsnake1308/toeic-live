import Urls from '/imports/api/common/api_url.js';

Meteor.methods({
  'five.get_questions': function() {
    try {
      var response = HTTP.call("GET", Urls.five_url()+'/question/get');
      return response.data.questions;
    } catch (e) {
      // Got a network error, time-out or HTTP error in the 400 or 500 range.
      return false;
    }
  }
})
