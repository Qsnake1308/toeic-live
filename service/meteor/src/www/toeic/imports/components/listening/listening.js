import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './listening.html';

Template.Listening.helpers({
  'listenings': [
    { id: 1, text: 'Listening 1' },
    { id: 2, text: 'Listening 2' },
    { id: 3, text: 'Listening 3' },
    { id: 4, text: 'Listening 4' },
  ],
});
