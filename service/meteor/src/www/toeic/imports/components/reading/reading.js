import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './reading.html';

Template.Reading.helpers({
  'readings': [
    { id: 1, text: 'Reading 1' },
    { id: 2, text: 'Reading 2' },
    { id: 3, text: 'Reading 3' },
    { id: 4, text: 'Reading 4' },
  ],
});
