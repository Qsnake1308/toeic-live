import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './part.html';

Template.Part.helpers({
  'parts': [
    { id: 1, text: 'Part 1' },
    { id: 2, text: 'Part 2' },
    { id: 3, text: 'Part 3' },
    { id: 4, text: 'Part 4' },
  ],
});
