import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './full.html';

Template.Full.helpers({
  'fulls': [
    { id: 1, text: 'Full 1' },
    { id: 2, text: 'Full 2' },
    { id: 3, text: 'Full 3' },
    { id: 4, text: 'Full 4' },
  ],
});
