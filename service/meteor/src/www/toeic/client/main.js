import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import '../imports/pages/home/home.js';
import '../imports/pages/desc/desc.js';
import '../imports/pages/part_five/part_five.js';
import '../imports/pages/part_six/part_six.js';
import '../imports/pages/part_seven/part_seven.js';
