from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from django.conf import settings
from wand.image import Image
from PIL import Image as PI

import pyocr
import pyocr.builders
import io
import PyPDF2

class Command(BaseCommand):
    PDF = "/opt/www/docser/pdfs/native/a.pdf"

    def handle(self, *args, **options):
        tool = pyocr.get_available_tools()[0]
        lang = tool.get_available_languages()[0]

        req_image = []
        final_text = []

        image_pdf = Image(filename=self.PDF, resolution=300)
        image_jpeg = image_pdf.convert('jpeg')

        for img in image_jpeg.sequence:
            img_page = Image(image=img)
            req_image.append(img_page.make_blob('jpeg'))

        for img in req_image:
            txt = tool.image_to_string(
                PI.open(io.BytesIO(img)),
                lang=lang,
                builder=pyocr.builders.TextBuilder()
            )
            final_text.append(txt)

        print final_text
        print "DONE <<<<<<<<<<<<<<<<<<"
