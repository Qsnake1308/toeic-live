from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from django.conf import settings

import os
import requests,json

class Command(BaseCommand):

    def handle(self, *args, **options):
        request_url = settings.PART_FIVE_URL+"/testing"
        response = requests.get(request_url)
        response = json.loads(response.content)
        print response
