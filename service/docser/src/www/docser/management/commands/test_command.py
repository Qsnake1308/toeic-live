from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from django.conf import settings

from wand.image import Image
from PIL import Image as PI
import pyocr
import pyocr.builders
import io
import PyPDF2

class Command(BaseCommand):

    def handle(self, *args, **options):
        path = '/opt/www/docser/pdfs/p7aswer1_ocr.pdf'

        pdfFileObj = open(path, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        pdfReader.numPages
        pageObj = pdfReader.getPage(0)
        try:
             text = pageObj.extractText().encode('utf-8')
             print text.decode('utf-8')
        except UnicodeDecodeError:
            pass

        print "DONE"

    # def handle(self, *args, **options):
    #     path = '/opt/www/docser/pdfs/p7aswer1_ocr.pdf'
    #
    #     tool = pyocr.get_available_tools()[0]
    #     lang = tool.get_available_languages()[0]
    #
    #     req_image = []
    #     final_text = []
    #     image_pdf = Image(filename=path, resolution=300)
    #     image_jpeg = image_pdf.convert('jpeg')
    #
    #     for img in image_jpeg.sequence:
    #         img_page = Image(image=img)
    #         req_image.append(img_page.make_blob('jpeg'))
    #
    #     for img in req_image:
    #         txt = tool.image_to_string(
    #             PI.open(io.BytesIO(img)),
    #             lang=lang,
    #             builder=pyocr.builders.TextBuilder()
    #         )
    #         final_text.append(txt)
    #
    #     print final_text
    #     print "DONE"
