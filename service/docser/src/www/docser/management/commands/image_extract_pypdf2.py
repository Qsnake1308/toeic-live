from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from django.conf import settings

from wand.image import Image
from PIL import Image as PI
import pyocr
import pyocr.builders
import io
import PyPDF2

class Command(BaseCommand):

    PDF = "/opt/www/docser/pdfs/native/a.pdf"

    def handle(self, *args, **options):
        pdfFileObj = open(self.PDF, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        pdfReader.numPages
        pageObj = pdfReader.getPage(0)
        try:
             text = pageObj.extractText().encode('utf-8')
             print text.decode('utf-8')
        except UnicodeDecodeError:
            pass

        print "DONE"
