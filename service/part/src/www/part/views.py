from django.shortcuts import render
from django.http import JsonResponse
from django.conf import settings

import json

def testing(request):
    response = {'message': "Hello"}
    return JsonResponse(response)

def insert_question(request, data): # data will be in json format
    response = {'data': data}
    return JsonResponse(response)

def insert_answer(request, data): # data will be in json format
    response = {'data': data}
    return JsonResponse(response)

def get_questions(request):
    response = {'questions': "Hello"}
    return JsonResponse(response)
