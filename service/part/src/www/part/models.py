from __future__ import unicode_literals

from django.db import models

# Create your models here.
## Part 1
class OneQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class OneAnswer(models.Model):
    question = models.ForeignKey(
        'OneQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)

##  Part 2
class TwoQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class TwoAnswer(models.Model):
    question = models.ForeignKey(
        'TwoQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)

##  Part 3
class ThreeQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class ThreeAnswer(models.Model):
    question = models.ForeignKey(
        'ThreeQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)

##  Part 4
class FourQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class FourAnswer(models.Model):
    question = models.ForeignKey(
        'FourQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)

##  Part 5
class FiveQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class FiveAnswer(models.Model):
    question = models.ForeignKey(
        'FiveQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)

##  Part 6
class SixQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class SixAnswer(models.Model):
    question = models.ForeignKey(
        'SixQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)

##  Part 7
class SevenQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class SevenAnswer(models.Model):
    question = models.ForeignKey(
        'SevenQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)
