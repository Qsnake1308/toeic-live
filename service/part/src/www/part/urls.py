from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^testing$', views.testing, name='testing'),
    url(r'^insert_question/(?P<data>[\w]*)$', views.insert_question, name='insert_question'),
    url(r'^insert_answer/(?P<data>[\w]*)$', views.insert_answer, name='insert_answer'),
    url(r'^question/get$', views.get_questions, name='get_questions'),
]
