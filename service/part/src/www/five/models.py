from __future__ import unicode_literals

from django.db import models

# Create your models here.
class PartOneQuestion(models.Model):
    question_text = models.CharField(max_length=255, default=None)
    explanation = models.TextField(default=None)

class PartOneAnswer(models.Model):
    question = models.ForeignKey(
        'PartOneQuestion',
        on_delete=models.CASCADE,
    )
    rank = models.IntegerField()
    answer_text = models.CharField(max_length=255, default=None)
    is_correct = models.BooleanField(default=False)
