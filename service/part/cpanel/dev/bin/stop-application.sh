## Load default config
. ./config.sh

## Stop services
sudo docker stop toeic-part-five-mysql
sudo docker stop toeic-part-five-web
